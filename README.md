# Welcome to Xpacebot!

This is a Telegram Bot that shows the user different frames of a SpaceX launch video to find the exact frame where lift-off takes place.

The bot is live in Telegram and can be contacted at [@xpacebot](https://t.me/xpacebot).

## Pre requisites

If you are setting up a new bot with [@BotFather](https://telegram.me/BotFather), make sure you enable the following settings:
- Nothing special for now

You also need to provide the token for the bot to use. It will be taken from the environment variable `TELEGRAM_TOKEN` or warn you if it's missing.

## Design choices

#### Day 1
My first attempt was to use BERNARD to implement a chatbot using its internal Telegram capabilities. I noticed it looks a little abandoned. I tried to install it and it didn't work. After digging in the repo, I found out it runs under Python 3.6, but the installed version in my system (Manjaro 20) was 3.8. After installing an alternative 3.6 under a `virtualenv`, I had to downgrade `pip` to v19.

Because of the `redis` dependency, I built everything using docker-compose. And then I read in the docs that BERNARD supports only text messages (which makes it not suitable for this use case).

#### Day 2
I have switched to `telepot`, and since I'm guessing this service is probably not going to be used by too many users, I went with `telepot`'s polling model running in a `virtualenv` with no `docker` required. If this were to be used by many users it would require more infrastructure which would allow us to use webhooks instead of polling, with a threaded web server maybe load balanced, and all the usual good stuff...

#### Day 3
After trying to deal with the bisection algorithm provided in the POC for a bit I realized I can't use it as is because I can't know the future. Telegram API does not allow editing Photo messages, a new message needs to be sent for every frame, but the previous one can be deleted to mimic the POC behavior.
I have built a stateless callback query function that can handle every step of the bisection independently, using simple json encoded DTOs to keep the management and consistency of this custom inner API easy. As a side effect, this enables the user to go back in their conversation with the bot and select a different answer to any previous query and keep going from there.
Since this diverges from the behavior of the POC, I have added an environment variable to toggle this: `DELETE_PREVIOUS_MESSAGE`. By default, previous messages are kept.

## Result

This is a screenshot of the final result.

![@xpacebot](screenshot.png)

## Improvements

I have tested this bot with the help of a friend in a multi user scenario, just to make sure nothieng leaks out of scope and I have improved the code based on their feedback.

I have not been able to find documentation on how to test a `telepot` bot (appart from `telepot`'s own tests, and they seem to require manual intervention). Manually testing and comparing with the POC yields the same results, so I'm calling it correct enough, but unit tests would be a necessary thing to do.

## Credits

Bot profile picture is "Space Core", from the Portal 2 video game. [This scene](https://www.youtube.com/watch?v=xeKMS62GrTI) is why I chose it. Image was taken from [here](https://half-life.fandom.com/wiki/Aperture_Science_Personality_Construct).
