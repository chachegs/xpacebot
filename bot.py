import os
from dataclasses_json import DataClassJsonMixin
from dataclasses import dataclass

from telepot import glance
from telepot.aio.helper import CallbackQueryOriginHandler, ChatHandler
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton

from framex import FrameX


@dataclass(frozen=True, unsafe_hash=True)
class QueryFrame(DataClassJsonMixin):
    """
    DTO class for easy management of callback query options
    """

    left: int
    right: int

    def pivot(self) -> int:
        """
        Calculates the pivot for this given bisection.
        :return:the pivot
        """
        return int((self.right + self.left) / 2)

    def is_final(self) -> bool:
        """
        :return: `True` if this is the final step.
        """
        return not ((self.left + 1) < self.right)


@dataclass(frozen=True, unsafe_hash=True)
class QueryFrameAnswer(QueryFrame):
    answer: bool


class Initiator(ChatHandler):
    """
    Handles general chat messages.
    """

    def __init__(self, *args, **kwargs):
        super(Initiator, self).__init__(*args, **kwargs)

    async def on_chat_message(self, msg):
        content_type, chat_type, chat_id = glance(msg)
        if content_type == "text":
            text = msg["text"]

            if text.startswith("/start"):
                await self.sender.sendMessage(
                    """Hi! I will show you a succession of pictures of a SpaceX launch and together we will determine the exact frame for take-off!!

Answer 'Yes' if you think the rocket has launched or 'No' it you think it hasn't.

Let's go!
""",
                    reply_markup=InlineKeyboardMarkup(
                        inline_keyboard=[
                            [InlineKeyboardButton(text="START", callback_data="start"),]
                        ]
                    ),
                )
                self.close()


class Bisector(CallbackQueryOriginHandler):
    """
    Handles all callback queries from the same origin.
    """

    def __init__(self, *args, **kwargs):
        super(Bisector, self).__init__(*args, **kwargs)
        self.api: FrameX = FrameX(
            os.getenv("API_BASE", "https://framex-dev.wadrid.net/api/"),
            os.getenv(
                "VIDEO_NAME", "Falcon Heavy Test Flight (Hosted Webcast)-wbSwFU6tY1c"
            ),
        )
        self._chat_id: str = None  # we need to wait to receive a message

    @property
    def count(self):
        return self.api.video.frames

    async def _show_frame(self, query: QueryFrame):
        """
        Sends the pivot frame of a `QueryFrame` to the user, with the appropriate markup.
        :param query:
        :return: None
        """

        if bool(os.getenv("DELETE_PREVIOUS_MESSAGE", "False")):
            await self.editor.deleteMessage()

        await self.bot.sendPhoto(
            self._chat_id,
            self.api.frame_url(query.pivot()),
            caption=f"{query.pivot()} - did the rocket launch yet?",
            reply_markup=InlineKeyboardMarkup(
                inline_keyboard=[
                    [
                        InlineKeyboardButton(
                            text="Yes",
                            callback_data=QueryFrameAnswer(
                                query.left, query.right, True
                            ).to_json(),
                        ),
                        InlineKeyboardButton(
                            text="No",
                            callback_data=QueryFrameAnswer(
                                query.left, query.right, False
                            ).to_json(),
                        ),
                    ]
                ]
            ),
        )

    async def _found(self, frame: int):
        """
        Sends the goal frame along with a descriptive text.
        :param frame: the frame number
        :return:
        """
        await self.bot.sendPhoto(
            self._chat_id,
            self.api.frame_url(frame),
            caption=f"Found! Take-off = {frame}",
            reply_markup=None,
        )

    async def on_callback_query(self, msg):
        """
        Handles responses to `InlineKeyboardMarkup` selections. Stateless
        :param msg: the received message
        :return:
        """
        query_id, from_id, query_data = glance(msg, flavor="callback_query")
        self._chat_id = msg["from"]["id"]

        # mimic the POC logic
        left = 0
        right = self.count - 1
        if query_data == "start":
            pass
        else:
            ans = QueryFrameAnswer.from_json(query_data)

            right = ans.right
            left = ans.left
            # mimic the POC logic
            if ans.answer:
                right = ans.pivot()
            else:
                left = ans.pivot()

        query = QueryFrame(right=right, left=left)
        if query.is_final():
            await self._found(query.right)
            self.close()

        await self._show_frame(query)
