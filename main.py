import os
import asyncio
import telepot
from telepot.aio.loop import MessageLoop
from telepot.aio.delegate import create_open, pave_event_space
from telepot.delegate import per_callback_query_origin, per_chat_id

from bot import Bisector, Initiator


def main(token):

    bot = telepot.aio.DelegatorBot(
        token,
        [
            pave_event_space()(per_chat_id(), create_open, Initiator, timeout=3),
            pave_event_space()(
                per_callback_query_origin(), create_open, Bisector, timeout=10
            ),
        ],
    )
    loop = asyncio.get_event_loop()

    loop.create_task(MessageLoop(bot).run_forever())
    print("Listening ...")

    loop.run_forever()


if __name__ == "__main__":
    if os.path.exists(".env") and os.getenv("TELEGRAM_TOKEN") is None:
        from dotenv import load_dotenv

        load_dotenv(".env")
        print("Loaded .env file")

    try:
        main(os.getenv("TELEGRAM_TOKEN"))
    except KeyboardInterrupt:
        pass
