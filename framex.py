from typing import Text
from urllib.parse import urljoin, quote

from httpx import Client
from typing import List, NamedTuple


class Video(NamedTuple):
    """
    That's a video from the API
    """

    name: Text
    width: int
    height: int
    frames: int
    frame_rate: List[int]
    url: Text
    first_frame: Text
    last_frame: Text


class FrameX:
    """
    Utility class to access the FrameX API
    """

    def __init__(self, base_url: str, video_name: str):
        """
        Initialize the FrameX API to a specific video.
        :param base_url: the base API URL
        :param video_name: the name of the video
        """
        self._base_url: str = base_url
        self._video_url: str = urljoin(self._base_url, f"video/{quote(video_name)}")
        self._video: Video = None
        self.client = Client()

    @property
    def video(self) -> Video:
        """
        Fetches information about a video and caches it
        """
        if self._video:
            return self._video

        r = self.client.get(f"{self._video_url}/?format=json")
        r.raise_for_status()
        self._video = Video(**r.json())
        return self.video

    def video_frame(self, frame: int) -> bytes:
        """
        Fetches the JPEG data of a single frame
        """
        r = self.client.get(self.frame_url(frame))
        r.raise_for_status()
        return r.content

    def frame_url(self, frame: int) -> str:
        """
        Builds the URL to a single frame
        :param frame: the frame number
        :return: the URL for the requested frame
        """
        return f"{self._video_url}/frame/{frame}/"
